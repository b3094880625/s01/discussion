import networkx as nx
import matplotlib.pyplot as plt

# type 1: Directed Graph
"""
graph = nx.DiGraph()
graph.add_edges_from([('a', 'b'), ('a', 'e'), ('b', 'c'), ('b', 'd'), ('d', 'e'), ('c', 'a')])
positioning = nx.spring_layout(graph, seed = 500)
nx.draw(graph, pos = positioning, with_labels = True)
nx.draw_networkx_nodes(graph, positioning, node_size = 300, node_color = "lightgray")

plt.savefig('directed.png')
plt.show()
"""

# type 2: Undirected Graph
'''
graph = nx.Graph()
graph.add_edges_from([('a', 'b'), ('a', 'e'), ('b', 'c'), ('b', 'd'), ('d', 'e'), ('c', 'a')])

positioning = nx.spring_layout(graph, seed = 500)
nx.draw(graph, pos = positioning, with_labels = True)
nx.draw_networkx_nodes(graph, positioning, node_size = 700, node_color = "lightgray")

plt.savefig('undirected.png')
plt.show()
'''

# type 3: Weighted Graph
'''
graph = nx.Graph()
graph.add_edge('a', 'b', weight=3.28)
graph.add_edge('b', 'c', weight=1.46)
graph.add_edge('c', 'd', weight=6.19)
graph.add_edge('d', 'a', weight=5.62)

pos = nx.spring_layout(graph, seed = 500)
nx.draw_networkx_nodes(graph, pos, node_size = 600, node_color = "lightgray")
nx.draw_networkx_edges(graph, pos, width = 6, edge_color = 'orange')
nx.draw_networkx_labels(graph, pos)
edge_labels = nx.get_edge_attributes(graph, 'weight')
nx.draw_networkx_edge_labels(graph, pos, edge_labels)

plt.box(False) # set to false to remove the border
plt.savefig('weighted')
plt.show()
'''

# type 4: Unweighted Graph
'''
graph = nx.Graph()

graph.add_edges_from([('J', 'P'), ('P', 'C'), ('C', 'M'), ('M', 'J')])
positioning = nx.spring_layout(graph, seed = 500)
nx.draw_networkx_nodes(graph, positioning, node_size = 600, node_color = "lightgray")

plt.savefig('unweighted')
plt.show()
'''

# type 5: Simple Graph
'''
G = nx.Graph()

# add edges
G.add_edge('Bus stop A', 'Bus stop B')
G.add_edge('Bus stop B', 'Bus stop C')
G.add_edge('Bus stop C', 'Bus stop D')

positioning = nx.spring_layout(G, seed = 500)
nx.draw(G, pos=positioning, with_labels=True)
nx.draw_networkx_nodes(G, positioning, node_size = 600, node_color = "lightgray")

plt.savefig('simple')
plt.show()
'''

# type 6: Multi Graph
'''
bridge = nx.MultiDiGraph()

# add edges
bridge.add_edges_from([('Computer A', 'Main Computer'),
	('Computer A', 'Computer B'),
	('Computer A', 'Computer C'),
	('Computer C', 'Computer D'),
	('Computer B', 'Computer E'),
	('Computer C', 'Computer C')])

bridge.add_edge('Main Computer', 'Computer A')

positioning = nx.spring_layout(bridge, seed = 1000)
nx.draw(bridge, pos=positioning, with_labels= True)
nx.draw_networkx_nodes(bridge, positioning, node_size = 400, node_color="lightgray")

plt.savefig('multigraph')
plt.show()
'''

# type 7: Finite Graph
'''
Demo = nx.Graph()
Demo.add_edges_from([('A', 'C'), ('B', 'C'), ('C', 'D'), ('D', 'E')])

positioning = nx.spring_layout(Demo, seed = 5000)
nx.draw(Demo, pos=positioning, with_labels=True)
nx.draw_networkx_nodes(Demo, positioning, node_size=600, node_color="lightgray")

plt.savefig('finite.png')
plt.show()
'''

# type 8: Infinite Graph - infinite number of vertex and edges

# type 9: Connected Graph
'''
gph = nx.Graph()

# declaring nodes
nodes = ['A', 'B', 'C', 'D', 'E']

# adding nodes to our graph
gph.add_nodes_from(nodes)

# declaring edges
edges = [('A', 'B'), ('B', 'C'), ('C', 'D'), ('D', 'E'), ('E', 'A')]

# adding edges to our graph
gph.add_edges_from(edges)

#making layouts for the connected graph
node_positions = nx.circular_layout(gph)

positioning = nx.spring_layout(gph, seed = 500)

nx.draw(gph, pos=node_positions, with_labels=True, node_size=600, node_color="lightgray")

plt.savefig('connected')
plt.show()
'''

# type 10: Disconnected Graph
'''
dgph = nx.Graph()

# declaring nodes
dnodes = ['Deneb', 'Vego', 'Altair', 'Alpheratz', 'Almach', 'Castor', 'Polux', 'Betelgeuse', 'Bellatrix', 'Alnilam', 'Saiph', 'Rigel', 'SRB 1806-20']

# adding nodes to our graph
dgph.add_nodes_from(dnodes)

# declaring edges
dedges = [('Deneb','Vego'),('Vego','Altair'), ('Alpheratz', 'Almach'),('Castor','Polux'), ('Betelgeuse', 'Bellatrix'),('Bellatrix','Alnilam'),('Betelgeuse','Alnilam'),('Saiph','Alnilam'),('Rigel','Alnilam')]

# adding edges to our graph
dgph.add_edges_from(dedges)

# making layout for the graphs
positioning = nx.circular_layout(dgph)

nx.draw(dgph, pos=positioning, with_labels=True)
nx.draw_networkx_nodes(dgph, positioning, node_size=600, node_color="lightgray")

plt.savefig('disconnected')
plt.show()
'''

# type 11: Complete Graph
'''
cgph3 = nx.Graph()

# declaring nodes
cnodes2 = [1,2,3,4,5,6,7]

# adding nodes to our graph
cgph3.add_nodes_from(cnodes2)

# declaring edges
cedges3 = [(1,2),(2,3),(3,4),(4,5),(5,6),(6,7),(7,1),(1,3),(1,4),(1,5),
        (1,6),(2,4),(2,5),(2,6),(2,7),(3,5),(3,6),(3,7),(4,5),(4,6),(4,7),(5,7)]

# adding edges to our graph
cgph3.add_edges_from(cedges3)

# making layout for our graph
positioning = nx.circular_layout(cgph3)
nx.draw(cgph3, positioning, with_labels=True)
nx.draw(cgph3, positioning, node_size=600, node_color="lightgray")

plt.savefig('complete')
plt.show()
'''

# type 12: Bipartite Graph
'''
bgph = nx.Graph()

# declaring nodes
X = [1,3,5,7]
Y = [2,4,6,8]

# adding nodes to our graph
bgph.add_nodes_from(X)
bgph.add_nodes_from(Y)

# declaring edges
bedges = [(1,8),(1,4),(1,6),(3,2),(3,6),(3,8),(5,8),(5,2),(5,4),(7,2),(7,4),(7,6)]

# adding edges to our graph
bgph.add_edges_from(bedges)

# setting our bipartite sets of nodes
X, Y = nx.bipartite.sets(bgph)

# setting dictionary to hold each sets
pos = {}

# loop thorugh our nodes and update their positioning in our dictionary
pos.update((n, (1, i)) for i, n in enumerate(X))
pos.update((n, (2, i)) for i, n in enumerate(Y))

nx.draw(bgph, pos=pos, labels={bnodes:bnodes for bnodes in bgph})

nx.draw_networkx_nodes(bgph, pos, node_size=600, node_color="lightgray")

plt.savefig('bipartite.png')
plt.show()
'''

# type 13: Null Graph
'''
ngph = nx.Graph()

# declaring nodes
nnodes = ['Phillipines', 'India', 'China']

# adding nodes to our graph
ngph.add_nodes_from(nnodes)

positioning = nx.spring_layout(ngph)
nx.draw(ngph, positioning, with_labels=True)
nx.draw_networkx_nodes(ngph, positioning, node_size=600, node_color="lightgray")

plt.savefig('null')
plt.show()
'''